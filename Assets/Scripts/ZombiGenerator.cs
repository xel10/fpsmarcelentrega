﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombiGenerator : MonoBehaviour
{

    public GameObject zombie;
    public Transform[] points;
    public float timeToSpawn;

    IEnumerator Start()
    {
        while(true)
        {
            yield return new WaitForSeconds(timeToSpawn);

            GameObject izombie = Instantiate(zombie);

            CreeperBehaviour cb = izombie.GetComponent<CreeperBehaviour>();
            cb.pathNodes = points;
        }
    }
}
