﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerLevels : MonoBehaviour
{

    // Use this for initialization
	private CapsuleCollider myCollider;


	void Awake(){
		myCollider = GetComponent<CapsuleCollider> ();


	}




	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "win")
	{

			SceneManager.LoadScene("Game2");
	}



		if (other.tag == "fall")
		{
			SceneManager.LoadScene("GameOverMenu");
		}


		if (other.tag == "Enemy")
		{


			Explode ();
		}

		if (other.tag == "winner")
		{

			SceneManager.LoadScene("WinGame");
		}
		if (other.tag == "vida")
		{


			gainlife ();
		}



	}

	private void Explode(){

		LifesManager.getInstance().LoseLive ();
		myCollider.enabled = false;


		Invoke ("Reset", 2);

	}

	private void Reset(){
		myCollider.enabled = true;

	}

	private void gainlife(){

		LifesManager.getInstance().GainLive ();
		myCollider.enabled = false;


		Invoke ("Reset", 2);

	}


}
