﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class LifesManager : MonoBehaviour {
	public Text LivesText;
	private int lives;

	// Use this for initialization
	private static LifesManager instance;
	void Awake(){



		if(instance == null){
			instance = this;

		}



	}




	void Start () {

		lives = 3;
		LivesText.text = "x " + lives.ToString ();



	}
	
	// Update is called once per frame
	public void LoseLive (){

		lives--;
		LivesText.text = "x " + lives.ToString ();


		if (lives <= -1) {

			SceneManager.LoadScene ("GameOverMenu");
		}


	}

	public static LifesManager getInstance(){
		return instance;

	}

	public void GainLive ()
	{

		lives++;
		LivesText.text = "x " + lives.ToString ();
	}
}




